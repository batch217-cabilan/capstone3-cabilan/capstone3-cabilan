import {Col, Row, Card} from "react-bootstrap"

export default function Highlights(){
    return(
        <div className="container-fluid pt-5">
        <Row className="my-3">
            <Col xs={12} md={4} className="margin">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Consumer Electronics</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula nibh ac eros efficitur ornare. In vitae mi nisi. Praesent quam eros, accumsan ac risus dapibus, ornare gravida purus. Pellentesque venenatis nec purus id porta. Quisque mi mauris,
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className="margin">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Home Appliances</h2>
                        </Card.Title>
                        <Card.Text>
                            vel, tristique ultricies arcu. Integer vestibulum scelerisque lectus, sit amet fermentum tellus porttitor eu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className="margin">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Gadgets, Toys, and DIY</h2>
                        </Card.Title>
                        <Card.Text>
                            Nunc sit amet dapibus orci, in fringilla urna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse quis est aliquam magna dictum scelerisque in in risus. Proin convallis sem leo, a volutpat nibh dignissim sed. Aliquam
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

             <Col xs={12} md={4} className="margin">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Personal Care</h2>
                        </Card.Title>
                        <Card.Text>
                            Aliquam faucibus nunc nec nisi mattis eleifend eget ac odio. Pellentesque facilisis id dui et tempus. Ut tincidunt libero nec nunc tristique, ac luctus risus blandit. Curabitur in nisl mattis, dictum tortor ac, cursus massa. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

             <Col xs={12} md={4} className="margin">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Furniture</h2>
                        </Card.Title>
                        <Card.Text>
                            Aliquam faucibus nunc nec nisi mattis eleifend eget ac odio. Pellentesque facilisis id dui et tempus. Ut tincidunt libero nec nunc tristique, ac luctus risus blandit. Curabitur in nisl mattis, dictum tortor ac, cursus massa. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

             <Col xs={12} md={4} className="margin">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Household Products</h2>
                        </Card.Title>
                        <Card.Text>
                            Aliquam faucibus nunc nec nisi mattis eleifend eget ac odio. Pellentesque facilisis id dui et tempus. Ut tincidunt libero nec nunc tristique, ac luctus risus blandit. Curabitur in nisl mattis, dictum tortor ac, cursus massa. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
           </div>
    )
}
