import { useContext } from "react"
import { NavLink } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar(){
    let userInfo = {
        token: localStorage.getItem("token"),
        id: localStorage.getItem("id"),
        isAdmin: localStorage.getItem("isAdmin")
      }

    const { user } = useContext(UserContext);
    console.log(user);

    if(userInfo.token === null && userInfo.id === null && userInfo.isAdmin === null ){
        return(
            <Navbar bg="secondary" expand="lg" variant="dark" className="fixed-top">
                <Container fluid>
                    <Navbar.Brand as={ NavLink } to="/">ED CABILAN</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                        <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                        <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        )
    }
    
    else if(userInfo.isAdmin === "true"){
        return (
            <Navbar bg="secondary" expand="lg" variant="dark" className="fixed-top">
                <Container fluid>
                    <Navbar.Brand as={ NavLink } to="/">ADMIN</Navbar.Brand>
                    <Nav.Link as={ NavLink } to="/products/all" end className="text-white"> Dashboard</Nav.Link>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>

                        <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        )
    }


    else {
        return (
            <Navbar bg="secondary" expand="lg" variant="dark" className="fixed-top">
            <Container fluid>
            <Navbar.Brand as={ NavLink } to="/">ED CABILAN</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
              <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
              <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
            </Nav>
            </Navbar.Collapse>
                </Container>
              </Navbar>
            )
            
            
    }


    
    
}